<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>task list</title>
    <link href="https://bootstrap-4.ru/docs/4.0/examples/sign-in/signin.css">
    <link rel="stylesheet"
          href="http://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script
            src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script
            src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <meta charset="UTF-8">
</head>
<body>
<div align="center">
<h3><a href="${pageContext.request.contextPath}/tasks/${projectId}?form">New Task</a></h3>
</div>
<div align="center">
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Title</th>
            <th scope="col">Description</th>
            <th scope="col">Date begin</th>
            <th scope="col">Date end</th>
            <th scope="col">Readiness status</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${taskList}" var="task">
            <tr>
                <td>${task.id}</td>
                <td>${task.name}</td>
                <td>${task.description}</td>
                <td>${task.dateBeginTask}</td>
                <td>${task.dateEndTask}</td>
                <td>${task.readinessStatus}</td>
                <td>
                    <a href="${pageContext.request.contextPath}/tasks/${projectId}/edit?id=${task.id}">Edit</a>

                    <a href="${pageContext.request.contextPath}/tasks/${projectId}/delete?id=${task.id}">Delete</a>

                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
</body>
</html>

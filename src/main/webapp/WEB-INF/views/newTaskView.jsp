
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>New task view </title>
    <link rel="stylesheet"
          href="http://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script
            src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script
            src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <meta charset="UTF-8">

    <style>
        html, body {
            width: 100%;
            height: 100%;
            margin: 0
        }

        #new_task_form {
            position: absolute;
            width: 400px;
            height: 400px;
            left: 40%;
            top: 30%;
            margin-left: -100px;
            margin-top: -100px;
            border: 1px solid
        }

        form {
            padding: 14px
        }
    </style>
</head>
<body>
<div id="new_task_form">
    <form:form method="POST" action="${pageContext.request.contextPath}/tasks/${projectId}?form">
    <div class="form-group">
        <label for="Title">Title</label>
        <input type="text" class="form-control" name="title" id="title" placeholder="Enter title"/>
    </div>
    <div class="form-group">
        <label for="description">Description</label>
        <input type="text" class="form-control" name="description" id="description" placeholder="Enter description"/>
    </div>
    <div class="form-group">
        <label for="dateBegin">Date begin</label>
        <input type="date" class="form-control" name="dateBegin" id="dateBegin"
               placeholder="Enter date begin"/>
    </div>
    <div class="form-group">
        <label for="dateBegin">Date end</label>
        <input type="date" class="form-control" name="dateEnd" id="dateEnd"
               placeholder="Enter date end"/>
    </div>
    <button type="submit" class="btn btn-primary">Create</button>
</div>
</form:form>
</body>
</html>

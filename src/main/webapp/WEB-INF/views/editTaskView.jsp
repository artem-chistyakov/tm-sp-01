
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>edit project form</title>
    <link rel="stylesheet"
          href="http://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script
            src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script
            src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <meta charset="UTF-8">

    <style>
        html, body {
            width: 100%;
            height: 100%;
            margin: 0
        }

        #edit_task_form {
            position: absolute;
            width: 400px;
            height: 400px;
            left: 40%;
            top: 30%;
            margin-left: -100px;
            margin-top: -100px;
            border: 1px solid
        }

        form {
            padding: 14px
        }
    </style>
</head>
<body>
<div id="edit_task_form">
    <form:form method="POST" action="${pageContext.request.contextPath}/tasks/${projectId}/edit?id=${editTask.id}"
               modelAttribute="editTask" >
    <div class="form-group">
        <label for="Title">New Title</label>
        <input type="text" class="form-control" name="title" id="title" value="${editTask.name}"/>
    </div>
    <div align="center" class="form-group">
        <label for="description">new description</label>
        <input type="text" class="form-control" name="description" id="description"
               value="${editTask.description}"/>
    </div>
    <div align="center" class="form-group">
        <label for="dateBegin">new date begin</label>
        <input type="date" class="form-control" name="dateBegin" id="dateBegin"
               value="${editTask.dateBeginTask}"/>
    </div>
    <div align="center" class="form-group">
        <label for="dateBegin">new date end</label>
        <input type="date" class="form-control" name="dateEnd" id="dateEnd"
               value="${editTask.dateEndTask}"/>
    </div>
        <button type="submit" class="btn btn-primary">Edit</button>
</div>
</form:form>
</body>
</html>

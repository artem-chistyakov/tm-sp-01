<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">

    <title>Project List</title>
    <link href="https://bootstrap-4.ru/docs/4.0/examples/sign-in/signin.css">
    <link rel="stylesheet"
          href="http://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script
            src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script
            src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <meta charset="UTF-8">
</head>
<body>
<div align="center">
    <%--    <h2>Customer Manager</h2>--%>
    <form method="get" action="search">
        <input type="text" name="keyword"/>
        <input type="submit" value="Search"/>
    </form>
    <h3><a href="${pageContext.request.contextPath}/projects/${userId}?form">New Project</a></h3>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Title</th>
            <th scope="col">Description</th>
            <th scope="col">Date begin</th>
            <th scope="col">Date end</th>
            <th scope="col">Readiness status</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${projectList}" var="project">
            <tr>
                <td>${project.id}</td>
                <td>${project.name}</td>
                <td>${project.description}</td>
                <td>${project.dateBeginProject}</td>
                <td>${project.dateEndProject}</td>
                <td>${project.readinessStatus}</td>
                <td>
                    <a href="${pageContext.request.contextPath}/projects/edit?id=${project.id}">Edit</a>

                    <a href="${pageContext.request.contextPath}/projects/delete?id=${project.id}">Delete</a>

                    <a href="${pageContext.request.contextPath}/tasks/${project.id}">Show tasks</a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <form:form action="${pageContext.request.contextPath}/logout" method="post">
        <input type="submit" value="Sign Out"/>
    </form:form>
</div>
</body>
</html>




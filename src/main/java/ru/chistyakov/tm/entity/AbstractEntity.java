package ru.chistyakov.tm.entity;


import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.UUID;

@MappedSuperclass
public class AbstractEntity {
    @NotNull
    @Id
    @Column(unique = true, name = "id",nullable = false,updatable = false)
    private String id;

    AbstractEntity() {
        id = UUID.randomUUID().toString();
    }

    @NotNull
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

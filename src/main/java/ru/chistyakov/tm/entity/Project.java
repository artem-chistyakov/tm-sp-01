package ru.chistyakov.tm.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.enumerate.ReadinessStatus;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "projects")
public class Project extends AbstractEntity {

    @Nullable
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false, updatable = false)
    private User user;

    @NotNull
    private String name = "";

    @NotNull
    @Column(name = "readiness_status")
    @Enumerated(EnumType.STRING)
    private ReadinessStatus readinessStatus = ReadinessStatus.PLANNED;
    @Nullable
    private String description;

    @Nullable
    @Column(name = "date_begin")
    private Date dateBeginProject;
    @Nullable
    @Column(name = "date_end")
    private Date dateEndProject;

    @Nullable
    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "project",
            cascade = CascadeType.ALL)
    private List<Task> tasks;
}

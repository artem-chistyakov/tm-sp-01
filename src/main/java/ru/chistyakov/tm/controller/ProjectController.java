package ru.chistyakov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.chistyakov.tm.api.IProjectService;
import ru.chistyakov.tm.api.IUserService;
import ru.chistyakov.tm.dto.ProjectDTO;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.entity.User;
import ru.chistyakov.tm.enumerate.ReadinessStatus;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/projects")
public class ProjectController {

    private IProjectService projectService;

    private IUserService userService;

    @Autowired
    public void setProjectService(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Autowired
    public void setUserService(IUserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public ModelAndView showListProjects() {
        final ModelAndView modelAndView = new ModelAndView("projectList");
        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        final String username = auth.getName();
        final User user = userService.findByUsername(username);
        final String userId = user.getId();
        final List<Project> projectList = projectService.findAllByUserId(userId);
        modelAndView.addObject("userId", userId);
        modelAndView.addObject("projectList", projectList);
        return modelAndView;
    }

    @NotNull
    @GetMapping(value = "/delete")
    public String deleteProject(@Nullable final @RequestParam("id") String projectId,@Nullable final RedirectAttributes ra) {
        projectService.remove(projectId);
        return "redirect:/projects/";
    }

    @NotNull
    @GetMapping(value = "/edit")
    public ModelAndView editProject(@RequestParam("id")@Nullable final String projectId,@Nullable final RedirectAttributes ra) {
        final Project project = projectService.findOne(projectId);
        final ModelAndView modelAndView = new ModelAndView("editProjectView");
        modelAndView.addObject("editProject", project);
        modelAndView.addObject("vars", Arrays.asList(ReadinessStatus.values()));
        return modelAndView;
    }

    @NotNull
    @PostMapping(value = "/edit")
    public String editProject(@RequestParam("id") final @Nullable String projectId,
                              @ModelAttribute(name = "title") @Nullable final String title,
                              @ModelAttribute(name = "description") @Nullable final String description,
                              @ModelAttribute(name = "dateBegin") @Nullable final Date dateBegin,
                              @ModelAttribute(name = "dateEnd") @Nullable final Date dateEnd,
                              @ModelAttribute(name = "status") @Nullable final String status,
                              final RedirectAttributes ra) {
        final Project editProject = projectService.findOne(projectId);
        editProject.setName(title);
        editProject.setDescription(description);
        editProject.setDateBeginProject(dateBegin);
        editProject.setDateEndProject(dateEnd);
//        editProject.setReadinessStatus(ReadinessStatus.valueOf(status));
        projectService.merge(editProject);
        return "redirect:/projects/";
    }

    @NotNull
    @GetMapping(value = "/{userId}", params = "form")
    public String newProject(@PathVariable("userId") final @Nullable String userId) {
        return "newProjectView";
    }

    @NotNull
    @PostMapping(value = "/{userId}", params = "form")
    public String newProject(@PathVariable("userId") String userId,
                             @ModelAttribute(name = "title") @Nullable final String title,
                             @ModelAttribute(name = "description") @Nullable final String description,
                             @ModelAttribute(name = "dateBegin") @Nullable final Date dateBegin,
                             @ModelAttribute(name = "dateEnd") @Nullable final Date dateEnd,
                             final RedirectAttributes ra) {
        final Project project = new Project();
        project.setName(title);
        project.setDescription(description);
        project.setDateBeginProject(dateBegin);
        project.setDateEndProject(dateEnd);
        final User user = userService.findById(userId);
        project.setUser(user);
        project.setReadinessStatus(ReadinessStatus.PLANNED);
        projectService.persist(project);
        return "redirect:/projects/";
    }

    @NotNull
    @PostMapping("/save")
    public String saveProject(@NotNull @RequestParam(name = "userId") final String userId, @ModelAttribute("newProject") ProjectDTO projectDTO) {
        projectDTO.setUserId(userId);
        projectService.persist(projectService.mapDtoToProject(projectDTO));
        return "redirect:/";
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }
}

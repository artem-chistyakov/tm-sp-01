package ru.chistyakov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.chistyakov.tm.api.IProjectService;
import ru.chistyakov.tm.api.ITaskService;
import ru.chistyakov.tm.api.IUserService;
import ru.chistyakov.tm.entity.Task;
import ru.chistyakov.tm.enumerate.ReadinessStatus;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/tasks")
public class TaskController {

    private ITaskService taskService;
    private IProjectService projectService;
    private IUserService userService;

    @Autowired
    public void setUserService(@NotNull final IUserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setProjectService(@NotNull final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Autowired
    public void setTaskService(@Nullable final ITaskService taskService) {
        this.taskService = taskService;
    }

    @NotNull
    @GetMapping(value = "/{projectId}")
    public ModelAndView showTasks(@Nullable @PathVariable(name = "projectId") final String projectId) {
        if (projectId == null) throw new IllegalArgumentException();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final List<Task> taskList = taskService.findByUsernameAndProjectId(authentication.getName(), projectId);
        final ModelAndView modelAndView = new ModelAndView("taskList");
        modelAndView.addObject("taskList", taskList);
        return modelAndView;
    }

    @NotNull
    @GetMapping(value = "/{projectId}", params = "form")
    public String newProject(@PathVariable("projectId") String projectId) {
        return "newTaskView";
    }

    @NotNull
    @PostMapping(value = "/{projectId}", params = "form")
    public String newProject(@PathVariable("projectId") String projectId,
                             @Nullable @ModelAttribute(name = "title") final String title,
                             @Nullable @ModelAttribute(name = "description") final String description,
                             @Nullable @ModelAttribute(name = "dateBegin") final Date dateBegin,
                             @Nullable @ModelAttribute(name = "dateEnd") final Date dateEnd,
                             final RedirectAttributes ra) {
        final Task task = new Task();
        task.setName(title);
        task.setDescription(description);
        task.setDateBeginTask(dateBegin);
        task.setDateEndTask(dateEnd);
        task.setReadinessStatus(ReadinessStatus.PLANNED);
        task.setProject(projectService.findOne(projectId));
        task.setUser(userService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName()));
        taskService.persist(task);
        return "redirect:/tasks/{projectId}";
    }

    @NotNull
    @GetMapping(value = "/{projectId}/edit")
    public ModelAndView editProject(@Nullable @PathVariable("projectId") String projectId,
                                    @Nullable @RequestParam("id") String taskId,
                                    final RedirectAttributes ra) {
        if (projectId == null) throw new IllegalArgumentException();
        final Task task = taskService.findOne(taskId);
        final ModelAndView modelAndView = new ModelAndView("editTaskView");
        modelAndView.addObject("projectId", projectId);
        modelAndView.addObject("editTask", task);
        modelAndView.addObject("vars", Arrays.asList(ReadinessStatus.values()));
        return modelAndView;
    }

    @NotNull
    @PostMapping(value = "/{projectId}/edit")
    public String editTask(@Nullable @PathVariable("projectId") String projectId,
                           @Nullable @RequestParam("id") String taskId,
                           @Nullable @ModelAttribute(name = "title") @NotNull final String title,
                           @Nullable @ModelAttribute(name = "description") final String description,
                           @Nullable @ModelAttribute(name = "dateBegin") final Date dateBegin,
                           @Nullable @ModelAttribute(name = "dateEnd") final Date dateEnd,
                           @Nullable @ModelAttribute(name = "status") final String status,
                           final RedirectAttributes ra) {
        final Task editTask = taskService.findOne(taskId);
        editTask.setName(title);
        editTask.setDescription(description);
        editTask.setDateBeginTask(dateBegin);
        editTask.setDateEndTask(dateEnd);
        taskService.merge(editTask);
        return "redirect:/tasks/" + projectId;
    }

    @NotNull
    @GetMapping(value = "/{projectId}/delete")
    public String deleteProject(@Nullable @PathVariable("projectId") final String projectId,
                                @Nullable @RequestParam("id") final String taskId,
                                final RedirectAttributes ra) {
        final Task task =
                taskService.findByUserIdAndTaskId(
                        userService.findByUsername(
                                SecurityContextHolder.getContext().getAuthentication().getName()).getId(), taskId);
        taskService.remove(task);
        return "redirect:/tasks/" + projectId;
    }
}

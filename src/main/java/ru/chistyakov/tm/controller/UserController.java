package ru.chistyakov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import ru.chistyakov.tm.api.IUserService;
import ru.chistyakov.tm.entity.User;
import ru.chistyakov.tm.enumerate.Role;

import java.util.Collections;

@Controller
public class UserController {

    private PasswordEncoder passwordEncoder;
    private IUserService userService;

    @Autowired
    public void setPasswordEncoder(@NotNull final PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    public void setUserService(@NotNull final IUserService userService) {
        this.userService = userService;
    }

    @NotNull
    @GetMapping("/registration")
    public String registration() {
        return "registration";
    }

    @NotNull
    @PostMapping("/registration")
    public String addUser(@Nullable final User user, @NotNull final Model model) {
        final User userFromDb = userService.findByUsername(user.getUsername());
        if (userFromDb != null) {
            model.addAttribute("message", "User exists");
            return "registration";
        }
        user.setActive(true);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRoles(Collections.singleton(Role.USER));
        System.out.println(user.getUsername());
        System.out.println(user.getPassword());
        userService.registryUser(user);
        return "login";
    }
}

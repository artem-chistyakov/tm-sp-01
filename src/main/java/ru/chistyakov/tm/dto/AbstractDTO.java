package ru.chistyakov.tm.dto;


import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.UUID;

@MappedSuperclass
public abstract class AbstractDTO implements Serializable {

    @NotNull
    @Getter
    @Setter
    @Id
    @Column(unique = true, name = "id")
    private String id;

    AbstractDTO() {
        id = UUID.randomUUID().toString();
    }
}

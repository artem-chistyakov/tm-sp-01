package ru.chistyakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.chistyakov.tm.entity.User;
import ru.chistyakov.tm.enumerate.Role;
import ru.chistyakov.tm.repository.IUserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service("userDetailsService")
public class UserDetailsServiceBean implements UserDetailsService {


    private IUserRepository userRepository;

    @Autowired
    public void setUserRepository(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(@NotNull final String username) throws UsernameNotFoundException {
        final User user = findByUsername(username);
        if (user == null) throw new UsernameNotFoundException("Пользователь не найден");
        org.springframework.security.core.userdetails.User.UserBuilder builder = null;
        builder = org.springframework.security.core.userdetails.User.withUsername(username);
        builder.password(user.getPassword());
        final Set<Role> userRoles = user.getRoles();
        final List<String> roles = new ArrayList<>();
        for (Role role : userRoles) roles.add(role.toString());
        builder.roles(roles.toArray(new String[]{}));
        return builder.build();
    }

    private User findByUsername(@NotNull final String username) {
        if (username == null || username.isEmpty()) return null;
        return userRepository.findUserByUsername(username);
    }
}

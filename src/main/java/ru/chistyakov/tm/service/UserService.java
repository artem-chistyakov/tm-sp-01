package ru.chistyakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.chistyakov.tm.dto.UserDTO;
import ru.chistyakov.tm.entity.User;
import ru.chistyakov.tm.repository.IProjectRepository;
import ru.chistyakov.tm.repository.ITaskRepository;
import ru.chistyakov.tm.repository.IUserRepository;

import java.util.Collection;

@Service
public class UserService implements ru.chistyakov.tm.api.IUserService {

    private IUserRepository userRepository;
    private ITaskRepository taskRepository;
    private IProjectRepository projectRepository;

    @Autowired
    public void setUserRepository(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setTaskRepository(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Autowired
    public void setProjectRepository(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    @Transactional
    public boolean updateUser(@Nullable final User user) {
        if (user == null) return false;
        return userRepository.updateUser(user.getId(), user.getUsername(), user.getPassword()) > 0;
    }

    @Override
    @Transactional
    public User registryUser(@Nullable User user) {
        if (user == null) return null;
        return userRepository.save(user);
    }

    @Override
    public @Nullable User findById(String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return userRepository.findById(userId).get();
    }

    @Override
    public void deleteAccount(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) return;
        userRepository.deleteById(userId);
    }

    public @Nullable User findByUsername(@Nullable final String login) {
        if (login == null) return null;
        else return userRepository.findUserByUsername(login);
    }

    @Override
    public @Nullable Collection<User> findAll() {
        return userRepository.findAll();
    }

    public @NotNull User mapUserDtoToUser(@NotNull final UserDTO userDTO) {
        final User user = new User();
        user.setId(userDTO.getId());
        user.setUsername(userDTO.getUsername());
        user.setPassword(userDTO.getPassword());
        user.setTasks(taskRepository.findByUserId(userDTO.getId()));
        user.setProjects(projectRepository.findByUserId(userDTO.getId()));
        return user;
    }

    public @NotNull UserDTO mapUserToUserDto(@NotNull final User user) {
        final UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setUsername(user.getUsername());
        userDTO.setPassword(user.getPassword());
        return userDTO;
    }
}
